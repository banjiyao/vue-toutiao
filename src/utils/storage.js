// 封装本地存储操作模块

// 存储 数据 将对象 格式为JSON 字符串来进行存储
export const setItem = (key, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}

// 获取数据

export const getItem = key => {
  const data = window.localStorage.getItem(key)
  try {
    console.log(data)
    return JSON.parse(data)
  } catch {
    return data
  }
}

// 删除数据
export const removerItem = key => {
  window.localStorage.removeItem(key)
}
