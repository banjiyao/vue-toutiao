import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login/login.vue'
import Layout from '@/views/Layout/layout'
import Home from '@/views/Home/home'
import Qa from '@/views/Qa/qa'
import Video from '@/views/Video/video'
import My from '@/views/My/my'

Vue.use(VueRouter)

const routes = [
  { path: '/login', component: Login },
  {
    path: '/',
    component: Layout,
    children: [
      { path: '', component: Home, name: 'home' },
      { path: '/qa', component: Qa, name: 'qa' },
      { path: '/video', component: Video, name: 'video' },
      { path: '/my', component: My, name: 'my' }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
