import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage'

Vue.use(Vuex)

const TOKEN_KEY = 'TOUTIAO_USER'
export default new Vuex.Store({
  state: {
    user: getItem(TOKEN_KEY)
    // 一个对象 存储当前用户信息 应用初始化的时候 从本地存储拿到数据
  },
  mutations: {
    setUser (state, obj) {
      console.log(obj)
      state.user = obj
      // 本地存储数据
      setItem(TOKEN_KEY, obj)
    }
  },
  actions: {
  },
  modules: {
  }
})
